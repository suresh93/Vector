SRC = $(wildcard src/*.c)
OBJ = $(SRC:.c=.o)
HEADER = $(wildcard include/*.h)
HTML = $(HEADER:.h=.html)

all: test

test: $(OBJ)
	make -C test
$(OBJ): $(HTML)
	make -C src
$(HTML):
	make -C doc
clean:
	rm lib/libvector.*
	rm src/*.o
	rm test/*.o
	rm doc/vector.*
	rm vector
