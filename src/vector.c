#include <vector_v2.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

int element_init(data_type type, generic_data user_value, data *element)
{
	element->field_type = type;
	element->field = user_value;

	return SUCCESS;
}

_vector vector(_vector *vector)
{
	vector->head = NULL;
	vector->tail = NULL;
	vector->append = &append;
	vector->prepend = &prepend;
	vector->insert = &insert;
	vector->truncat = &truncat;
	vector->behead = &behead;
	vector->remov = &remov;
	vector->set = &set;
	vector->get = &get;
	vector->is_empty = &is_empty;
	vector->move = &move;
	vector->first = &first;
	vector->last = &last;
	vector->size = &size;
	vector->splice = &splice;
	vector->clear = &clear;
}

int append(_vector *vector, data element)
{
	node *new_node = (node *)malloc(sizeof(node));

	if (new_node == NULL)
		return FAILURE;
	new_node->next = NULL;
	new_node->prev = NULL;
	memmove(&new_node->element, &element, sizeof(data));

	if (vector->tail == NULL) {
		vector->head = new_node;
		vector->tail = new_node;
		return SUCCESS;
	}
	vector->tail->next = new_node;
	new_node->prev = vector->tail;
	vector->tail = new_node;
	return SUCCESS;
}

int prepend(_vector *vector, data element)
{
	node *new_node = (node *)malloc(sizeof(node));

	if (new_node == NULL)
		return FAILURE;

	new_node->next = NULL;
	new_node->prev = NULL;
	memmove(&new_node->element, &element, sizeof(data));

	if (vector->head == NULL) {
		vector->head = new_node;
		vector->tail = new_node;
		return SUCCESS;
	}
	vector->head->prev = new_node;
	new_node->next = vector->head;
	vector->head = new_node;
	return SUCCESS;
}

int insert(_vector *vector, int pos, data element)
{
        int tot_count = 1;
        node *new_node = malloc(sizeof(node));
        node *start_pos = vector->head;
        node *curr_pos;

        new_node->next = NULL;
        new_node->prev = NULL;
	memmove(&new_node->element, &element, sizeof(data));

        if (new_node == NULL)
                return FAILURE;
        for (tot_count = 1; vector->head != NULL; vector->head = vector->head->next, tot_count++)
                if (vector->head->next == NULL)
                        break;

        vector->head = start_pos;

        if (pos < 1 || pos > tot_count + 1)
                return FAILURE;

        if (vector->head == NULL || pos == 1) {
                if (prepend(vector, element))
                        return FAILURE;
		free(new_node);
                return SUCCESS;
        }
        if (pos == tot_count + 1) {
                if (append(vector, element))
                        return FAILURE;
		free(new_node);
                return SUCCESS;
        }

        if (pos > (tot_count / 2))
                for (curr_pos = vector->tail; pos != tot_count; curr_pos = curr_pos->prev,
tot_count--);
        else
                for (tot_count = 1, curr_pos = vector->head; tot_count != pos; curr_pos =
curr_pos->next, tot_count++);

        new_node->next = curr_pos;
        new_node->prev = curr_pos->prev;
        new_node->prev->next = new_node;
        curr_pos->prev = new_node;

        return SUCCESS;
}

int truncat(_vector *vector)
{
        node *prev_node;

        if (vector->tail == NULL)
                return FAILURE;

        if (vector->tail->prev == NULL) {
                free(vector->tail);
                vector->tail = NULL;
                vector->head = NULL;
                return SUCCESS;
        }

        prev_node = vector->tail->prev;

        prev_node->next = NULL;
        free(vector->tail);
        vector->tail = prev_node;
        return SUCCESS;
}

int behead(_vector *vector)
{
        node *next_node;

        if (vector->head == NULL)
                return FAILURE;

        if (vector->head->next == NULL) {
                free(vector->head);
                vector->tail = NULL;
                vector->head = NULL;
                return SUCCESS;
        }

        next_node = vector->head->next;

        next_node->prev = NULL;
        free(vector->head);
        vector->head = next_node;

        return SUCCESS;
}

int remov(_vector *vector, int pos)
{
        node *start_pos = vector->head;
        node *curr_pos;
        int tot_count = 1;

	if (vector->head == NULL)
                return FAILURE;

        for (tot_count = 1; vector->head != NULL; vector->head = vector->head->next, tot_count++)
                if (vector->head->next == NULL)
                        break;

	vector->head = start_pos;

	if (pos < 1 || pos > tot_count)
                return FAILURE;

        if (pos == 1) {
                if (behead(vector))
                        return FAILURE;
                return SUCCESS;
        }

        if (pos == tot_count) {
                if (truncat(vector))
                        return FAILURE;
                return SUCCESS;
        }

        if (pos > (tot_count / 2))
                for (curr_pos = vector->tail; pos != tot_count; curr_pos = curr_pos->prev,
tot_count--);
        else
                for (tot_count = 1, curr_pos = vector->head; tot_count != pos; curr_pos = curr_pos->next, tot_count++);

        curr_pos->prev->next = curr_pos->next;
        curr_pos->next->prev = curr_pos->prev;
        free(curr_pos);

        return SUCCESS;
}

int set(_vector *vector, int pos, data element)
{
	node *start_pos = vector->head;
	node *curr_pos;
	int tot_count = 1;

	if (vector->head == NULL)
		return FAILURE;
	for (tot_count = 1; vector->head != NULL; vector->head = vector->head->next, tot_count++)
		if (vector->head->next == NULL)
			break;

	vector->head = start_pos;
	
	if (pos < 1 || pos > tot_count)
		return FAILURE;

	if (pos > (tot_count / 2))
		for (curr_pos = vector->tail; pos != tot_count; curr_pos = curr_pos->prev, tot_count--);
	else
		for (tot_count = 1, curr_pos = vector->head; tot_count != pos; curr_pos = curr_pos->next, tot_count++);

	memmove(&curr_pos->element, &element, sizeof(data));
	return SUCCESS;
}

int get(_vector *vector, int pos, data *element)
{
	node *start_pos = vector->head;
	node *curr_pos;
	int tot_count = 1;

	for (tot_count = 1; vector->head != NULL; vector->head = vector->head->next, tot_count++)
		if (vector->head->next == NULL)
			break;
	vector->head = start_pos;

	if (pos < 1 || pos > tot_count)
		return FAILURE;
	if (vector->head == NULL)
		return FAILURE;
	if (pos > (tot_count / 2))
		for (curr_pos = vector->tail; pos != tot_count; curr_pos = curr_pos->prev, tot_count--);
	else
		for (tot_count = 1, curr_pos = vector->head; tot_count != pos; curr_pos = curr_pos->next, tot_count++);
	memmove(element, &curr_pos->element, sizeof(data));
	return SUCCESS;
}

int is_empty(_vector *vector)
{
	int tot_count = 1;
	node *start_pos = vector->head;

	if (vector->head == NULL)
		return FAILURE;
	for (tot_count = 1; vector->head != NULL; vector->head = vector->head->next, tot_count++)
		if (vector->head->next == NULL)
			break;
	vector->head = start_pos;

	return SUCCESS;
}

int move(_vector *vector, int old_pos, int new_pos)
{
	node *start_pos = vector->head;
	data element;
	int tot_count = 1;

	if (vector->head == NULL)
		return FAILURE;

	for (tot_count = 1; vector->head != NULL; vector->head = vector->head->next, tot_count++)
		if (vector->head->next == NULL)
			break;

	vector->head = start_pos;

	if (old_pos < 1 || old_pos > tot_count || new_pos < 1 || new_pos > tot_count)
		return FAILURE;

	if (get(vector, old_pos, &element))
	    return FAILURE;
	if (remov(vector, old_pos))
		return FAILURE;
	if (insert(vector, new_pos, element))
		return FAILURE;

	return SUCCESS;
}

int first(_vector *vector, data *element)
{
	if (vector->head == NULL)
		return FAILURE;
	memmove(element, &(vector->head->element), sizeof(data));
	return SUCCESS;
}

int last (_vector *vector, data *element)
{
	if (vector->tail == NULL)
		return FAILURE;
	memmove(element, &(vector->tail->element), sizeof(data));
	return SUCCESS;
}

int size(_vector *vector)
{
	int tot_count = 1;
	node *start_pos = vector->head;

	if (vector->head == NULL)
		return FAILURE;
	for (tot_count = 1; vector->head != NULL; vector->head = vector->head->next, tot_count++)
		if (vector->head->next == NULL)
			break;
	vector->head = start_pos;
	return tot_count;
}

_vector *splice(_vector *old_vector, int pos)
{
	_vector *new_vector = malloc(sizeof(_vector));
	node *start_pos = old_vector->head;
	node *curr_pos;
	int tot_count = 1;

	for (tot_count = 1; old_vector->head != NULL; old_vector->head = old_vector->head->next, tot_count++)
		if (old_vector->head->next == NULL)
			break;

	old_vector->head = start_pos;

	if (pos < 1 || pos > tot_count) {
		free(new_vector);
		return NULL;
	}
	if (old_vector->head == NULL) {
		free(new_vector);
		return NULL;
	}

	vector(new_vector);
	
	old_vector->head = start_pos;

	if (pos > (tot_count / 2))
		for (curr_pos = old_vector->tail; pos != tot_count; curr_pos = curr_pos->prev, tot_count--);
	else
		for (tot_count = 1, curr_pos = old_vector->head; tot_count != pos; curr_pos = curr_pos->next, tot_count++);

	new_vector->head = curr_pos;
	new_vector->head->prev = NULL;
	new_vector->tail = old_vector->tail;

	if (pos == 1) {
		old_vector->head = NULL;
		old_vector->tail = NULL;
		return new_vector;
	}
	old_vector->tail = new_vector->head->prev;
	old_vector->tail->next = NULL;
	return new_vector;
}

int clear(_vector *vector)
{
	node *tmp = vector->head;

	while (tmp != NULL) {
		tmp = tmp->next;
		free(tmp);
	}

	free(vector->head);
	vector->head = NULL;
	vector->tail = NULL;
	return SUCCESS;
}
