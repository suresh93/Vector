#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <error.h>
#include <vector_v2.h>

int sort(_vector *v)
{
	int i;
	int j;
	data element_1;
	data element_2;

	if (v->is_empty(v))
		return FAILURE;
	for (i = 1; i < v->size(v); i++) {
		for (j = 1; j < v->size(v); j++) {
			if (v->get(v, j, &element_1))
				return FAILURE;
			if (v->get(v, j + 1, &element_2))
				return FAILURE;
			if (*(int *)&element_1.field > *(int *)&element_2.field) {
				v->move(v, j, j + 1);
			}
		}
	}
	return SUCCESS;
}

int main(void)
{
	_vector v = vector(&v);
	int i;
	int data;
	node *entry;
	struct element element;

	for (i = 0; i < 6; i++) {
		printf("Enter data ");
		scanf("%d", &data);
		ELEMENT(INT, data, element);
		v.append(&v, element);
	}
	if (sort(&v))
		error(FAILURE, errno, "FAILED\n");
	for_each(element, v) {
		printf("Type - %d, Data - %d\n", VALUE(element), TYPE(element));
	}
	return SUCCESS;
}
