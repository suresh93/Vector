#include <stdbool.h>
#ifndef __VECTOR_H__
#define __VECTOR_H__
#define FAILURE -1
#define SUCCESS 0
#define ELEMENT(type, data, element) element_init(type, (generic_data)data, &element)
#define VALUE(entry)  entry->element.field
#define TYPE(entry)   entry->element.field_type
#define for_each(entry, vector) for(node * entry = vector.head; entry != NULL; entry = entry->next)
#define for_each_rev(entry, vector) for(node * entry = vector.tail; entry != NULL; entry = entry->prev)

typedef union g_datatype {
	char char_g;
	short short_g;
	int int_g;
	long long_g;
	float float_g;
	double double_g;
} generic_data;

typedef enum g_typename {
	CHAR = 1,
	SHORT,
	INT,
	LONG,
	FLOAT,
	DOUBLE
} data_type;

typedef struct element {
	generic_data field;
	data_type field_type;
} data;

typedef struct vector_node {
	data element;
	struct vector_node *next;
	struct vector_node *prev;       
} node;

typedef struct vector {
	node *head;
	node *tail;

	int (*append)(struct vector *vector, data element);
	int (*prepend)(struct vector *vector, data element);
	int (*insert)(struct vector *vector, int pos, data element);

	int (*truncat)(struct vector *vector);
	int (*behead)(struct vector *vector); 
	int (*remov)(struct vector *vector, int pos);

	int (*set)(struct vector *vector, int pos, data element);
	int (*get)(struct vector *vector, int pos, data *element);
	int (*is_empty)(struct vector *vector);

	int (*first)(struct vector *vector, data *element);
	int (*last)(struct vector *vector, data *element);
	int (*clear)(struct vector *vector);
	int (*destruct)(struct vector *vector);
	int (*size)(struct vector *vector);
	int (*move)(struct vector *vector, int old_pos, int new_pos);
	struct vector* (*splice)(struct vector *vector, int pos);
} _vector;

/**
 * vector() - Initialize a Vector
 * @vector: The vector user passes.
 *
 * Initializes the Vector. Should be invoked before any other
 * vector interface functions are invoked.
 *
 * Return: The vector is returned back.
 */
_vector vector(_vector *vector);

/**
 * element_init() - Initializes the element.
 * @type: The type of data.
 * @user_value: The data to be stored.
 * @element: The structure going to contain data and type.
 *
 * Initializes an element to be inserted into the node by getting
 * the data and its type from the user.
 *
 * Return: Status if function succeeded or failed.
 */
int element_init(data_type type, generic_data user_value, data *element);

/**
 * append() - Append a node to Vector.
 * @vector: The Vector created by user.
 * @element: The data to be inserted in node.
 *
 * Inserts a node at the end of the Vector each time.
 * 
 * Return: Status if the function succeeded or failed
 */
int append(_vector *vector, data element);

/**
 * prepend() - Prepend a node to the Vector.
 * @vector: The Vector where node is to be inserted.
 * @element: The data to be inserted in node.
 *
 * Prepend inserts a node at the beginning of the vector
 * each time its invoked.
 *
 * Return: Status if the function succeeded or failed.
 */
int prepend(_vector *vector, data element);

/**
 * insert() - Insert a node in the Vector.
 * @vector: The Vector where node is to be inserted.
 * @pos: The position in Vector where the node is to be inserted.
 * @element: The data to be inserted in the node.
 *
 * Inserts a node in the position the user has requested.
 *
 * Return: Status if the function succeeded or failed.
 */
int insert(_vector *vector, int pos, data element);

/**
 * truncat() - Deletes the last node.
 * @vector: The Vector where all nodes are present.
 *
 * The last node in the Vector is removed each time the
 * function is invoked.
 *
 * Return: Status if the function succeeded or failed.
 */
int truncat(_vector *vector);

/**
 * behead() - Deletes the first node.
 * @vector: The Vector where all nodes are present
 *
 * The last node in the Vector is removed each time the
 * function is invoked.
 *
 * Return: Status if the function succeeded or failed.
 */
int behead(_vector *vector);

/**
 * remov() - Deletes a node.
 * @vector: Vector passed by the user.
 * @pos: Position of the node.
 *
 * The the position of the node given by the user is
 * deleted each time this function is invoked.
 *
 * Return: Status if the function succeeded or failed.
 */
int remov(_vector *vector, int pos);

/**
 * clear() - Clears the Vector.
 * @vector: Vector passed by the user.
 *
 * The Vector the user passes is cleared of all nodes
 * and made empty.
 *
 * Return: Status if the function succeeded or failed.
 */
int clear(_vector *vector);

/**
 * move() - Moves a node to a position
 * @vector: Vector passed by the user.
 * @old_pos: Node to be moved's position.
 * @new_pos: New position the node is to be moved.
 *
 * The node from the old position is removed and moved
 * to the new position inside the vector.
 *
 * Return: Status if the function succeeded or failed.
 */
int move(_vector *vector, int old_pos, int new_pos);

/**
 * set() - Sets the data of a node.
 * @vector: Vector the user passes.
 * @pos: The node's position.
 * @element: The element to be inserted.
 *
 * The element passed by the user is copied into the node
 * present in the position specified by the user.
 *
 * Return: Status if the function succeeded or failed.
 */
int set(_vector *vector, int pos, data element);

/**
 * first() - Get first node
 * @vector: The vector passed by user.
 * @element: The element in the node.
 *
 * Get's the first node's element.
 *
 * Return: Status if the function succeeded or failed.
 */
int first(_vector *vector, data *element);

/**
 * last() - Get last node. 
 * @vector: The vector passed by user.
 * @element: The element in the node.
 *
 * Get's the last node's element.
 *
 * Return: Status if the function succeeded or failed.
 */
int last(_vector *vector, data *element);

/**
 * get() - Get the value of node.
 * @vector: The vector passed by used
 * @pos: Node's position.
 * @element: The data to be copied into.
 *
 * The value of a node present in the position
 * specified by the user is copied into the element.
 *
 * Return: Status if the function succeeded or failed.
 */
int get(_vector *vector, int pos, data *element);

/**
 * is_empty() - Check if Vector is empty
 * @vector: The Vector passed by the user.
 *
 * Checks if the Vector passed by the user is empty
 * or contains any node.
 *
 * Return: If the vector is empty or not.
 */
int is_empty(_vector *vector);

/**
 * size() - Calculate size of vector
 * @vector: The Vector passed by user.
 *
 * The total count of all nodes inside the vector is
 * calculated.
 *
 * Return: The size of the vector
 */
int size(_vector *vector);

/**
 * splice() - Splices a vector into two.
 * @vector: The Vector passed by user
 * @pos: Position to splice from
 *
 * The Vector passed by user is split into two
 * from the position specified by the user
 *
 * Return: The New Vector created after the splice.
 */
_vector * splice(_vector *vector, int pos);

#endif /* __VECTOR_H__ */
